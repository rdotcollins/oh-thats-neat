---
title: "About"
date: 2022-12-11T21:37:58+00:00
type: "page"
---

I'm Rita. I like to poke at computers and write here about the things I make. 
I know python almost as well as I know English, but I also know C, C++, Matlab, SQL, Haskell, Julia and Danish!
I'm also a transgender woman with autism who has helped run community peer support groups for other transgender people in the past.
I have formal education in physics, data science, and wrote my masters thesis in process mining.
I'm here to help computers, data and the world make just a bit more sense to people. 
I can be found on https://www.linkedin.com/in/rita-collins-4379b112b/ 


