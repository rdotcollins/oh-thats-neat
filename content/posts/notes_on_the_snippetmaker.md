+++
title = "rstudio snippets to YASnippets!"
author = ["river"]
date = 2020-09-08T00:00:00+01:00
draft = false
+++

## Necessity {#necessity}

{{< figure src="/img/pipeexpand.gif" >}}

One day, on a sleepy August afternoon, I was trying to search through my old notes on how
"tidy" `R` worked after a while spent in python-data-land.
I like being able to speak fluently in both, leveraging both for the sake of flexibility, but
sometimes lose sight of the syntax of both. Having snippets of code on-hand is a very useful thing
I've been using for years with [Yasnippet](https://github.com/joaotavora/yasnippet) but I noticed something odd:

I had no snippets for R?!
This had to change.
Remembering that R-studio had a snippet system of its own, I looked at
the syntax of the average snippet.
I grabbed a handful of snippets from search results on github for Rstudio
(credit for the original snippets go to <https://github.com/gadenbuie/snippets>)

```text
snippet aa
	${1:var} <- ${1:var} %>%
	  ${0}

snippet gp "ggplot point"
	ggplot(${1:data}, aes(${2:x}, ${3:y})) + geom_point()${0}

snippet pm "purrr::map()"
	purrr::map(${0})

```

Hmm.
Dollar signs and curly braces? `$0` used to indicate where the cursor should land after expanding `$1`, `$2`, ... `$n`?
Named expansions named `${n:name}`?
This seems very close to YASnippet's syntax.
For comparison, a python snippet.

```text
# -*- mode: snippet -*-
# name: tryelse
# key: try
# --
try:
    $0
except $1:
    $2
else:
    $3
```

Some key differences existed:

-   YASnippet had one snippet per file, Rstudio had one snippet per language
-   Each YASnippet file required a key (the abbreviation used to call the snippet inline) and a name (what the snippet is called)
-   Each Rstudio snippet was defined like a python function (not like an R function, strangely): all code was indented by one tab (`\t`) and finished off with a double newline (`\n\n`).
-   Rstudio sometimes specified `${VISUAL}` to act as a wrappable snippet, something YASnippet does by default.
-   Rstudio can call R code inline in a snippet, something I'm not planning on implementing here (R and elisp, despite having interesting common ancestry, are worlds apart).

And so, always being down for a bit of a regex puzzle, I got to work making a conversion script.
Something I could run from the shell and let multiple files through... assuming I could find more.
I wrote it in python, though a part of me wishes I used elisp to make it more emacs-friendly.
After all, YASnippet is ultimately an Emacs plugin.

Ah well. Here we are.


## ... is the mother of invention {#dot-dot-dot-is-the-mother-of-invention}

Here's the script. Its a python 3 script you can call as

```bash
python ./rstudiotoyasnippet.py #<files go here>
```

```python
#!/usr/bin/env python
#cute script to turn r studio snippets to yasnippet snippets
#works in linux, needs some minor tweaks (replace string paths with os.path stuff)
#assume we're putting in R code, not sure how useful this will be for other languages that
#rstudio can handle.
import os
import sys
import re

#a template for the snippet as required by YASnippet
snipmatter ="""# -*- mode: snippet -*-
# name: {1}
# key: {0}
# group: R
# --
{2}
"""


```

Heres a string I'm going to call the `.format()` string method on later, inserting the
key, name, and code itself in {0}, {1}, and {2} respectively.

Here I make sure files exist and can be accessed.

```python
files_to_import = sys.argv[1:]
#for each file...
for arg in files_to_import:
    try:
        arg_base = os.path.basename(arg)
        snipfilename = arg_base[:arg_base.index(".")]
    except ValueError: #if theres a ValueError, that means theres no file extension...
        snipfilename = os.path.basename(arg)  # - which is handlable.
    try:
        #try and dump the whole file to a string
        snippetfile = open(arg)
        snippetfilestring = snippetfile.read()
    except FileNotFoundError as err: #if the file doesnt exist, continue.
        print(err,"\n","attempting to continue to next file")
        continue
    #once we have a file from which to extract snippets from, we now have a well defined filename.
    #and now we'll make it a directory!
    folderpath = "./{}".format(snipfilename)
    if os.path.isdir(folderpath) == False:
        os.mkdir(folderpath)
```

In addition to all of this, I define a function that'll help us sanitise some strings later.
This will allow us to make names for our files that don't have any special characters in them.

| string         | alnum(string) |
|----------------|---------------|
| asdf()         | asdf          |
| foo{"bar":baz} | foobarbaz     |

The python string method `is.alnum()` came to mind - so I wrote a function to remove any
non-alphanumeric characters. Turned out messy in the end (due to python shallow copying lists by default)

```python

def alnumfy(string):
    #turns a string into a nice friendly identifier-like string.
    lstring = list(string)
    outstring = lstring[:] #a deep copy, shallow copies get awkward here
    for c in lstring:
        if not c.isidentifier():
            outstring.remove(c)
    out = ''.join(outstring)
    return(out)
```

From this point on, I'm going to leave my comments in the code here.


### Regular Expressions - now with 99% more perspiration! {#regular-expressions-now-with-99-more-perspiration}

At one stage, I thought it would be sufficient to build a regular expression to parse the whole file.

```python
#they're split by double line breaks generally.
#ACTUALLY NO
#THEYRE DELIMITED BY "SNIPPET <key> "<name>"
#drop anything that isnt explicitly a snippet
#this doesnt work, if there's no name in the snippet, it puts the code in group 2
snip_regex = r'snippet (.+)\s*"?(.*)"?\n(.*)\n\n'
#this might have worked if python would allow for matching a group (group 3) multiple times...
#sadly, it just scoops up first line of code and stops.
```

However, I soon found myself in choppy waters.

```python
snip_regex1 = r'snippet (\w+)\s"?([\w\: ]*)"?\n\t(.+)\n\n'

```

I ran into issue after issue trying to parse the code. After all, its just a set of strings... right?
A snippet starts with the word snippet, and ends with a particular pattern of whitespace... right?

```python
#this is so SO close - but you cant just exclude the word "snippet" from \S.
#it starts eating into the next snippet :(
snip_regex2 = r'snippet(.*)\n([\t\S\n ]+)'

```

Then it hit me.
Just as a CSV is a comma-delimited file
and a TSV is a tab-delimited file
these snippets... they were like delimited by the word "snippet".
I had a "snippet delimited file".
The key wasn't in capturing the right whitespace, it was in splitting the strings!

Python's regex not only has symbols that match the starts and ends of **lines** `^` and `$` in a string...
but also has symbols that match the starts and ends of **strings** `\A` and `\Z`!

Yes, its probably slower as we're performing more matches, but it's a python script you might run once or twice, you can wait a few milliseconds.

```python

    snippet_raw_list = snippetfilestring.split("snippet")
    snippet_list = []
    for sn in snippet_raw_list:
        sn = sn.strip() #remove trailing whitespace
        #todo: find a more elegant solution to getting rid of # blocks than just nuking the comments.
        sn = re.sub(r'#.+',"",sn,re.MULTILINE) #get rid of comments
        snip_match = re.match(r'^([\w]+[\S]*)[ \t]*(.*)[\t ]*$\s*((?s:.+\Z))',sn,re.M)
        try:
            snip_tuple = (snip_match[1],snip_match[2],snip_match[3])
            snippet_list.append(snip_tuple)
        except TypeError:
            print("oops, no match here")

```

Now we should have a triple `(key,name,code)` per file.

```python
    #iterate through the list of r-studio snippets
    #and translate them into a yasnippet freindly format.
    for st in snippet_list:
        yas_output_key = st[0]
        #i don't like the idea of having spaces in the filenames.
        #also given some of the rstudio snippets have no names, we have to make some up.
        if st[1] == '':
            #if no name is given, make one up!
            #staple together the key and the few characters of the code.
            #we need an identifiable but filesystem friendly name, so no weird symbols.
            yas_output_name = alnumfy(st[0]+st[2][0:2].replace(" ","_").replace('"',""))
        else:
            yas_output_name = alnumfy(st[1].replace(" ","_").replace('"',""))


        untranslated_code = st[2].replace("VISUAL","0").splitlines()
        translated_code = ""

        for line in untranslated_code:
            line = line.replace("\t","",1) #replace the first tab in each line
            translated_code = translated_code + line + "\n" # not as fast as .join() but its clear right?

```

The reason we sanitised the snippet name is hopefully clear now: we're going to use it as the filename for the YASnippet snippet itself.
And so finally, fill in the format string defined before all this looping, and define a filename.

```python
        snipmatter_filled = snipmatter.format(yas_output_key,yas_output_name,translated_code)
        yas_output_filename = folderpath + "/" + yas_output_name #generally has no file extension
        with open(yas_output_filename,"w") as yas_output_file:
            yas_output_file.write(snipmatter_filled)
    print("finished with ",snipfilename)
    snippetfile.close()

```


## And it works! {#and-it-works}

```sh
#this returns some of my R snippets now.
ls /home/river/.emacs.d/snippets/r/ | head
```

```text
a
aa
aq
bind_cols
bind_rows
Create_test
Describe_that
docoptru
element_blank
element_line
```

Meaning I can pipe and mutate to my hearts content.
