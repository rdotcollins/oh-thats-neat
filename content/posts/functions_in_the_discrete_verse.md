+++
title = "Adventures on Z_n"
author = ["Ri\\*"]
date = 2022-09-12T00:00:00+01:00
draft = false
+++

## How to be a fussy about functions: a visual guide. {#how-to-be-a-fussy-about-functions-a-visual-guide-dot}

It pays to pay attention to functions when working with small sets.
These ubiquitous lil subsets of <img src="/ltximg/functions_in_the_discrete_verse_65ada6f152810ac705cb10f251f8f6f703636540.png" alt="$S \times T$">
A typical way to define a function f from a set <img src="/ltximg/functions_in_the_discrete_verse_5d650f96e75c65eef1726b6201a05e4386f21549.png" alt="$S$">, called the domain of the
function, to a set <img src="/ltximg/functions_in_the_discrete_verse_48e890fda086410e5753084e9c248e63cba8df00.png" alt="$T$">, called the range, is that <img src="/ltximg/functions_in_the_discrete_verse_c2b477c3c6a68ae2b48840c2901388e050859600.png" alt="$f$"> is a relationship between <img src="/ltximg/functions_in_the_discrete_verse_5d650f96e75c65eef1726b6201a05e4386f21549.png" alt="$S$"> to <img src="/ltximg/functions_in_the_discrete_verse_48e890fda086410e5753084e9c248e63cba8df00.png" alt="$T$"> that
relates one and only one member of <img src="/ltximg/functions_in_the_discrete_verse_48e890fda086410e5753084e9c248e63cba8df00.png" alt="$T$"> to each element of <img src="/ltximg/functions_in_the_discrete_verse_5d650f96e75c65eef1726b6201a05e4386f21549.png" alt="$S$">.
We use <img src="/ltximg/functions_in_the_discrete_verse_33edd27ecb34b3285c086fec27994ce72eaef929.png" alt="$f(x)$"> to stand for the element of <img src="/ltximg/functions_in_the_discrete_verse_48e890fda086410e5753084e9c248e63cba8df00.png" alt="$T$"> that is related to the element <img src="/ltximg/functions_in_the_discrete_verse_f84f7234a9590c394ac10525a371a38e50bd864f.png" alt="$x$"> of <img src="/ltximg/functions_in_the_discrete_verse_5d650f96e75c65eef1726b6201a05e4386f21549.png" alt="$S$">.

Here I work through a few appendix problems from Combinatorics Through Guided Discovery by Kenneth P. Bogart and show how I can use and org-mode to make executable, plaintext documents that'll render your maths homework in style.
Much of the stuff in this post is new to me, groups, morphisms, <img src="/ltximg/functions_in_the_discrete_verse_7bfc19265656173159e86b00efe278842d0f28f5.png" alt="$TikZ$"> included.
I started this blog to showcase where I'm going, and I where I want to go, I want to understand what it means for something to have structure, mathematically.
Eventually, I can start writing about what that means statistically and even programmatically. But first, I'm showing what I've learned over the last while.
While I could post my entire set of notes on this gorgeous book, they're not all great. Most of it is chickenscratch, but some of it is this kind of quality.
Consider it a tentative preview of more things I think are neat.


### Problem 333. {#problem-333-dot}

   Draw the digraph of the relation from the set <img src="/ltximg/functions_in_the_discrete_verse_302fcb5e87b9fbb569560abe46a5c81e9e8fffa0.png" alt="$\{A, M, P, S\}$"> to
the set <img src="/ltximg/functions_in_the_discrete_verse_f6d6f5bb36012edc95a1c76fb0609b300a3f75ab.png" alt="$\{\text{Sam, Mary, Pat, Ann, Polly, Sarah}\}$"> given by “is the first letter of.”

I'm using [Sage](https://www.sagemath.org/) here, which does a great job of sprucing up your python experience with familiar mathematical concepts and operations defined on sets and the like.

```sage
#333
names = set("Sam,Mary,Pat,Ann,Polly,Sarah".split(","))
#is the first letter of
firstletters = set("AMPS")
result = {}
for x in firstletters:
    #slow is smooth, smooth is fastqqwq
    result[x] = []

for x in firstletters:
    for y in names:
        if y[0] == x:
            result[x].append(y)
            #doing the coords like this stops the nodes from going *everywhere* in sage's plotting thing
#posns1 = {a:[i,0] for i,a in enumerate(names)}
#posns2 = {b:[j,1] for j,b in enumerate(firstletters)}
#kwargs
posns={**posns1,**posns2}

d = DiGraph(result)
#d.view(partition=list(firstletters),
#       save_pos=True,
#       pos=posns,
#       vertex_size=700,figsize=(8,8))
d.set_latex_options(format="dot2tex")
print(latex(d))
```


<div class="equation-container">
<span class="equation">
<img src="/ltximg/functions_in_the_discrete_verse_e57ef182bd1bbd82f8e6e9b32fa9abf8602ccf32.png" alt="\begin{tikzpicture}[&amp;gt;=latex,line join=bevel,]
%%
\node (node_0) at (16.5bp,7.5bp) [draw,draw=none] {$\text{\texttt{Sarah}}$};
  \node (node_1) at (125.5bp,57.5bp) [draw,draw=none] {$\text{\texttt{P}}$};
  \node (node_2) at (102.5bp,7.5bp) [draw,draw=none] {$\text{\texttt{Pat}}$};
  \node (node_6) at (148.5bp,7.5bp) [draw,draw=none] {$\text{\texttt{Polly}}$};
  \node (node_3) at (194.5bp,57.5bp) [draw,draw=none] {$\text{\texttt{A}}$};
  \node (node_5) at (194.5bp,7.5bp) [draw,draw=none] {$\text{\texttt{Ann}}$};
  \node (node_4) at (237.5bp,7.5bp) [draw,draw=none] {$\text{\texttt{Mary}}$};
  \node (node_7) at (237.5bp,57.5bp) [draw,draw=none] {$\text{\texttt{M}}$};
  \node (node_8) at (39.5bp,57.5bp) [draw,draw=none] {$\text{\texttt{S}}$};
  \node (node_9) at (62.5bp,7.5bp) [draw,draw=none] {$\text{\texttt{Sam}}$};
  \draw [black,-&amp;gt;] (node_1) ..controls (119.67bp,44.338bp) and (114.18bp,32.868bp)  .. (node_2);
  \draw [black,-&amp;gt;] (node_1) ..controls (131.22bp,44.553bp) and (136.47bp,33.603bp)  .. (node_6);
  \draw [black,-&amp;gt;] (node_3) ..controls (194.5bp,44.553bp) and (194.5bp,33.603bp)  .. (node_5);
  \draw [black,-&amp;gt;] (node_7) ..controls (237.5bp,44.761bp) and (237.5bp,34.304bp)  .. (node_4);
  \draw [black,-&amp;gt;] (node_8) ..controls (33.672bp,44.338bp) and (28.176bp,32.868bp)  .. (node_0);
  \draw [black,-&amp;gt;] (node_8) ..controls (45.328bp,44.338bp) and (50.824bp,32.868bp)  .. (node_9);
%
\end{tikzpicture}
">
</span>
<span class="equation-label">
1
</span>
</div>

This is **not** a function.

When we talk about functions generally here, we're pointing at **exactly one object**
Cos otherwise, if we were say
"P is the first letter of" - we'd have more than one answer!
we're asserting here that when we say <img src="/ltximg/functions_in_the_discrete_verse_d1cdbaedbc32b02a824a777a36fb86d12963b18c.png" alt="$f(x) = y$">, that there is
one "x" going in, one "y" coming out.

Remember. **There is no such thing as a multi-valued function unless its returning a set or tuple.**


### Problem 334 {#problem-334}

   Draw the digraph of the relation from the set <img src="/ltximg/functions_in_the_discrete_verse_302fcb5e87b9fbb569560abe46a5c81e9e8fffa0.png" alt="$\{A, M, P, S\}$"> to
the set <img src="/ltximg/functions_in_the_discrete_verse_f6d6f5bb36012edc95a1c76fb0609b300a3f75ab.png" alt="$\{\text{Sam, Mary, Pat, Ann, Polly, Sarah}\}$"> given by “has as it's first letter”

```sage
#334
names = set("Sam,Mary,Pat,Ann,Polly,Sarah".split(","))
firstletters = set("AMPS")
result = {}
for name in names:
    result[name] = []

#slow is smooth, smooth is fast. yes im looping twice, but this is instructive.
for name in names:
    for symb in firstletters:
        if symb == name[0]:
            result[name].append(symb)

d = DiGraph(result)

d.set_latex_options(format="dot2tex")
print(latex(d))
```


<div class="equation-container">
<span class="equation">
<img src="/ltximg/functions_in_the_discrete_verse_302e1a843eb5f67244a2d72ea2bab08e6b2a9cda.png" alt="\begin{tikzpicture}[&amp;gt;=latex,line join=bevel,]
%%
\node (node_0) at (16.5bp,56.5bp) [draw,draw=none] {$\text{\texttt{Sarah}}$};
  \node (node_8) at (39.5bp,6.5bp) [draw,draw=none] {$\text{\texttt{S}}$};
  \node (node_1) at (125.5bp,6.5bp) [draw,draw=none] {$\text{\texttt{P}}$};
  \node (node_2) at (102.5bp,56.5bp) [draw,draw=none] {$\text{\texttt{Pat}}$};
  \node (node_3) at (194.5bp,6.5bp) [draw,draw=none] {$\text{\texttt{A}}$};
  \node (node_4) at (237.5bp,56.5bp) [draw,draw=none] {$\text{\texttt{Mary}}$};
  \node (node_7) at (237.5bp,6.5bp) [draw,draw=none] {$\text{\texttt{M}}$};
  \node (node_5) at (194.5bp,56.5bp) [draw,draw=none] {$\text{\texttt{Ann}}$};
  \node (node_6) at (148.5bp,56.5bp) [draw,draw=none] {$\text{\texttt{Polly}}$};
  \node (node_9) at (62.5bp,56.5bp) [draw,draw=none] {$\text{\texttt{Sam}}$};
  \draw [black,-&amp;gt;] (node_0) ..controls (22.328bp,43.338bp) and (27.824bp,31.868bp)  .. (node_8);
  \draw [black,-&amp;gt;] (node_2) ..controls (108.33bp,43.338bp) and (113.82bp,31.868bp)  .. (node_1);
  \draw [black,-&amp;gt;] (node_4) ..controls (237.5bp,42.234bp) and (237.5bp,31.91bp)  .. (node_7);
  \draw [black,-&amp;gt;] (node_5) ..controls (194.5bp,43.553bp) and (194.5bp,32.603bp)  .. (node_3);
  \draw [black,-&amp;gt;] (node_6) ..controls (142.04bp,42.012bp) and (136.86bp,31.21bp)  .. (node_1);
  \draw [black,-&amp;gt;] (node_9) ..controls (56.672bp,43.338bp) and (51.176bp,31.868bp)  .. (node_8);
%
\end{tikzpicture}
">
</span>
<span class="equation-label">
1
</span>
</div>


<div class="equation-container">
<span class="equation">
<img src="/ltximg/functions_in_the_discrete_verse_efdf9b905ff950e0d876999ab6754574e57e2bf3.png" alt="\begin{tikzpicture}[&amp;gt;=latex,line join=bevel,]
\node (node_0) at (11.0bp,56.5bp) [draw,draw=none] {$\text{Sam}}$};
  \node (node_8) at (34.0bp,6.5bp) [draw,draw=none] {asdhjk};
  \node (node_1) at (2,1) [draw,draw=none] {$\text{\texttt{Pat}}$};
  \node (node_2) at (126.0bp,6.5bp) [draw,draw=none] {$\text{\texttt{P}}$};
\end{tikzpicture}
">
</span>
<span class="equation-label">
1
</span>
</div>

**This** is a function.


### Problem 335 {#problem-335}

Draw the digraph of the relation on the set <img src="/ltximg/functions_in_the_discrete_verse_f6d6f5bb36012edc95a1c76fb0609b300a3f75ab.png" alt="$\{\text{Sam, Mary, Pat, Ann, Polly, Sarah}\}$"> **to itself** given by “has the same first letter as.”

```sage
#335
names = set("Sam,Mary,Pat,Ann,Polly,Sarah".split(","))
firstletters = set("AMPS")

result = {}
for name in names:
    result[name] = []

for name in names:
    result[name] = [n for n in names if n[0]==name[0]]

d = DiGraph(result)

d.set_latex_options(format="dot2tex")
print(latex(d))
```


<div class="equation-container">
<span class="equation">
<img src="/ltximg/functions_in_the_discrete_verse_cef7f056acb6b343c4135ed697a365cb5c3df3ae.png" alt="\begin{tikzpicture}[&amp;gt;=latex,line join=bevel,]
%%
\node (node_0) at (16.5bp,60.488bp) [draw,draw=none] {$\text{\texttt{Sarah}}$};
  \node (node_5) at (16.5bp,9.4882bp) [draw,draw=none] {$\text{\texttt{Sam}}$};
  \node (node_1) at (80.5bp,60.488bp) [draw,draw=none] {$\text{\texttt{Pat}}$};
  \node (node_4) at (80.5bp,9.4882bp) [draw,draw=none] {$\text{\texttt{Polly}}$};
  \node (node_2) at (141.5bp,60.488bp) [draw,draw=none] {$\text{\texttt{Mary}}$};
  \node (node_3) at (202.5bp,60.488bp) [draw,draw=none] {$\text{\texttt{Ann}}$};
  \draw [black,-&amp;gt;] (node_0) ..controls (37.241bp,72.347bp) and (51.0bp,70.156bp)  .. (51.0bp,60.488bp) .. controls (51.0bp,53.464bp) and (43.737bp,50.387bp)  .. (node_0);
  \draw [black,-&amp;gt;] (node_0) ..controls (9.8625bp,47.148bp) and (9.2465bp,35.62bp)  .. (node_5);
  \draw [black,-&amp;gt;] (node_1) ..controls (97.935bp,72.347bp) and (109.5bp,70.156bp)  .. (109.5bp,60.488bp) .. controls (109.5bp,53.993bp) and (104.28bp,50.872bp)  .. (node_1);
  \draw [black,-&amp;gt;] (node_1) ..controls (73.954bp,47.433bp) and (73.302bp,36.592bp)  .. (node_4);
  \draw [black,-&amp;gt;] (node_2) ..controls (162.48bp,71.999bp) and (173.5bp,69.473bp)  .. (173.5bp,60.488bp) .. controls (173.5bp,54.452bp) and (168.53bp,51.33bp)  .. (node_2);
  \draw [black,-&amp;gt;] (node_3) ..controls (219.93bp,72.347bp) and (231.5bp,70.156bp)  .. (231.5bp,60.488bp) .. controls (231.5bp,53.993bp) and (226.28bp,50.872bp)  .. (node_3);
  \draw [black,-&amp;gt;] (node_4) ..controls (87.209bp,24.153bp) and (87.706bp,34.973bp)  .. (node_1);
  \draw [black,-&amp;gt;] (node_4) ..controls (103.12bp,20.999bp) and (115.0bp,18.473bp)  .. (115.0bp,9.4882bp) .. controls (115.0bp,3.3114bp) and (109.38bp,0.18685bp)  .. (node_4);
  \draw [black,-&amp;gt;] (node_5) ..controls (23.126bp,22.732bp) and (23.752bp,34.25bp)  .. (node_0);
  \draw [black,-&amp;gt;] (node_5) ..controls (33.935bp,21.347bp) and (45.5bp,19.156bp)  .. (45.5bp,9.4882bp) .. controls (45.5bp,2.9925bp) and (40.279bp,-0.12766bp)  .. (node_5);
%
\end{tikzpicture}
">
</span>
<span class="equation-label">
1
</span>
</div>


### Problem 337 {#problem-337}

Digraphs of functions help us to visualize whether or not they are onto or one-to-one.
For example,
let both <img src="/ltximg/functions_in_the_discrete_verse_5d650f96e75c65eef1726b6201a05e4386f21549.png" alt="$S$"> and <img src="/ltximg/functions_in_the_discrete_verse_48e890fda086410e5753084e9c248e63cba8df00.png" alt="$T$"> be the sets <img src="/ltximg/functions_in_the_discrete_verse_e5b4646fb69d6af3dd21f1644766e39b711ff02e.png" alt="$\{−2, −1, 0, 1, 2\}$"> and
let <img src="/ltximg/functions_in_the_discrete_verse_b883bb042f11836413a595491a744b40447e6824.png" alt="$S^\prime$"> and <img src="/ltximg/functions_in_the_discrete_verse_17c00b76c90eb0044c8aec65c16d5899bd8d0f2c.png" alt="$T^\prime$"> be the set <img src="/ltximg/functions_in_the_discrete_verse_d39671f137cf0f4a545e11f4ee0ee5ae1323c5cc.png" alt="$\{0, 1, 2\}$">. Let <img src="/ltximg/functions_in_the_discrete_verse_d4fa590c3d3c644f8f555f107aa747f8b33003de.png" alt="$f(x) = 2 − \|x\|$">.

```sage
S = Set(range(-2,2+1))
T = Set(range(-2,2+1))
import networkx as nx
#this is a hack
def eff(n):

    return(2-abs(n))

def f_digraph(func,domain,rang):
    #this works assuming that each application of eff to an element of its domain produces
    #EXACTLY ONE value.
    dig = {str(e) : [str(func(e))+"_"] for e in domain}
    if rang == None:
        rang = Set(str(r)+"_" for r in domain)
    else:
        rang = Set(str(r)+"_" for r in rang)
        image = Set(r[0] for r in dig.values())
        notimage = Set(k for k in rang if k not in image)
    for k in notimage:
        dig.update({k:[]})

    #print("domain = {}\nrange = {}\nimage={},notimage={}".format(domain,rang,image,notimage))
    #print(posns)
    #rang = {str(e) : [str(func(e))+"\'"] for e in domain}
    #domain_indexed = [i+1 for i in range(len(domain))]

    d1 = nx.from_dict_of_lists(dig,create_using=nx.DiGraph)

    d2 = DiGraph(d1,pos=posns,format="NX",vertex_labels=True)

    return(dig,d1,d2)





Sp = Set(eff(m) for m in S)
Tp = Set(eff(m) for m in S)

s2sp = f_digraph(eff,S,Sp)
d = s2sp[2]
d.set_latex_options(format="dot2tex")
print(latex(d))
#this f is not injective, but it is surjective
#
#
```


<div class="equation-container">
<span class="equation">
<img src="/ltximg/functions_in_the_discrete_verse_e7d02f200698eb586f2acf15c5e33c97ad8a8fd0.png" alt="\begin{tikzpicture}[&amp;gt;=latex,line join=bevel,]
%%
\node (node_0) at (8.5bp,57.0bp) [draw,draw=none] {$0$};
  \node (node_5) at (8.5bp,7.0bp) [draw,draw=none] {$\text{\texttt{2{\char`\_}}}$};
  \node (node_1) at (38.5bp,57.0bp) [draw,draw=none] {$1$};
  \node (node_6) at (64.5bp,7.0bp) [draw,draw=none] {$\text{\texttt{1{\char`\_}}}$};
  \node (node_2) at (106.5bp,57.0bp) [draw,draw=none] {$2$};
  \node (node_7) at (113.5bp,7.0bp) [draw,draw=none] {$\text{\texttt{0{\char`\_}}}$};
  \node (node_3) at (140.5bp,57.0bp) [draw,draw=none] {$-2$};
  \node (node_4) at (72.5bp,57.0bp) [draw,draw=none] {$-1$};
  \draw [black,-&amp;gt;] (node_0) ..controls (8.5bp,44.122bp) and (8.5bp,33.338bp)  .. (node_5);
  \draw [black,-&amp;gt;] (node_1) ..controls (45.087bp,43.839bp) and (51.299bp,32.37bp)  .. (node_6);
  \draw [black,-&amp;gt;] (node_2) ..controls (108.23bp,44.122bp) and (109.8bp,33.338bp)  .. (node_7);
  \draw [black,-&amp;gt;] (node_3) ..controls (133.19bp,43.004bp) and (127.05bp,32.094bp)  .. (node_7);
  \draw [black,-&amp;gt;] (node_4) ..controls (70.37bp,43.221bp) and (68.632bp,32.793bp)  .. (node_6);
%
\end{tikzpicture}
">
</span>
<span class="equation-label">
1
</span>
</div>

So in terms of the magnitudes of S and T:

injective
: for a function to be injective, <img src="/ltximg/functions_in_the_discrete_verse_b4dc78d593023989584c851d682e5a7a6fcc4327.png" alt="$|S| &amp;lt;= |T|$">, and each element in <img src="/ltximg/functions_in_the_discrete_verse_5d650f96e75c65eef1726b6201a05e4386f21549.png" alt="$S$"> goes to a different <img src="/ltximg/functions_in_the_discrete_verse_48e890fda086410e5753084e9c248e63cba8df00.png" alt="$T$">

surjective
: for a function to be surjective, <img src="/ltximg/functions_in_the_discrete_verse_806693fe5b34ff9c1e250c978f2e78d510cc6634.png" alt="$|S| =&amp;gt; |T|$"> and each element of <img src="/ltximg/functions_in_the_discrete_verse_48e890fda086410e5753084e9c248e63cba8df00.png" alt="$T$"> could have came from <img src="/ltximg/functions_in_the_discrete_verse_5d650f96e75c65eef1726b6201a05e4386f21549.png" alt="$S$"> through <img src="/ltximg/functions_in_the_discrete_verse_c2b477c3c6a68ae2b48840c2901388e050859600.png" alt="$f$">

bijective
: is both injective and surjective, and <img src="/ltximg/functions_in_the_discrete_verse_41d8d058b348e0b04d215f513c62cc71f7f73752.png" alt="$|S| = |T|$">


## Exercise 8.4c, Visual Group theory {#exercise-8-dot-4c-visual-group-theory}


#### what are all the homomorphisms <img src="/ltximg/functions_in_the_discrete_verse_f7c60419ca206a9a3fb9a0cf341d460e6d8a0d57.png" alt="$h_i : C_4 \rightarrow C_4$">? {#what-are-all-the-homomorphisms-h-i-c-4-rightarrow-c-4}

Okay so, what are homomorphisms?
they're functions defined on groups that have to obey two big constraints.

1.  <img src="/ltximg/functions_in_the_discrete_verse_57df67d4b5fdac5e8bc0cebc2461bbd7d862a8bd.png" alt="$h(xy) = h(x) \cdot h(y)$">
2.  which means that homomorphisms on groups are defined by their generators, as
    <img src="/ltximg/functions_in_the_discrete_verse_9aa739e624426a50656f79d9a93b391ebdd1ad08.png" alt="$h(x)\cdot e = h(x)\cdot h(e) =h(x)= h(xe)$">
3.  which kinda implies <img src="/ltximg/functions_in_the_discrete_verse_f604021be8119b044299b5f78a10fe08ca0ccf7c.png" alt="$h(e) = e$">

we can id them by what they do to the generators of the group.
defining <img src="/ltximg/functions_in_the_discrete_verse_ccd685503dbaaf0e98aa48565417d996ca448dd1.png" alt="$C_4 = \mathbb{Z}/4\mathbb{Z} = (\{0,1,2,3\},+)$"> (the integers\* mod 4).

In the picture above, we have our domain, the orange <img src="/ltximg/functions_in_the_discrete_verse_6dfab97b56a0d0d34f7a132afd27c15c8bafa601.png" alt="$C_4$"> and the codomain purple <img src="/ltximg/functions_in_the_discrete_verse_6dfab97b56a0d0d34f7a132afd27c15c8bafa601.png" alt="$C_4$">.
How, pictorially, do we make a homomorphism?

-   Link the identity in the domain to the identity in the codomain, satisfying constraint 2 above.
-   Link the rest of the arrows in such a way that the elements linked in the codomain form something that looks like a group, satisfying constraint 1 above.


<div class="equation-container">
<span class="equation">
<img src="/ltximg/functions_in_the_discrete_verse_f971098dabb33f819d40e435b96b054fc91dba74.png" alt="\begin{tikzpicture}[
vertex1/.style={draw,thin,circle,fill=Bisque,minimum size=14pt,inner sep=0pt},
vertex2/.style={draw,thin,circle,fill=Thistle,minimum size=14pt,inner sep=0pt},
action/.style={draw=VioletRed,thick,&amp;gt;={Stealth[round]},edge label=\color{VioletRed}$h_{?}$},
action1/.style={draw=red,thick,&amp;gt;={Stealth[round]}},
action2/.style={draw=blue,thick,&amp;gt;={Stealth[round]}},
background rectangle/.style={fill=white},
show background rectangle]
%  \draw[help lines] (-1,2) grid (4,-2);
\graph[math nodes]{
  {[clockwise,n=4,nodes={vertex1},edges=action1] 0,1,2,3; 0-&amp;gt;1-&amp;gt;2-&amp;gt;3-&amp;gt;0},
  {[clockwise,n=4,nodes={vertex2,xshift=3cm},edges=action2] 0',1',2',3'; 0'-&amp;gt;1'-&amp;gt;2'-&amp;gt;3'-&amp;gt;0'},
  {[edges=action]
    0 -&amp;gt; 0'}
};
%after much stack exchange
%https://tex.stackexchange.com/questions/52157/tikz-automata-make-an-arrow-to-nowhere
\draw [-&amp;gt;] (1) edge [bend left,action] ++ (0.3,0.5);
\draw [-&amp;gt;] (3) edge [bend left,action] ++ (0.3,0.5);
\draw [-&amp;gt;] (2) edge [bend left,action] ++ (0.3,0.5);
%\draw [-&amp;gt;] (1) edge   [bend left,action]  (3')
% \draw [-&amp;gt;,action] (1) to[loop above] node[] {} (1);
%\draw [-&amp;gt;,action] (1) to[bend left] node[] {} ++ (1.6,-1.5);
\end{tikzpicture}
">
</span>
<span class="equation-label">
1
</span>
</div>

Firstly: lets do the identity bijection (an isomorphism!), mapping everything on the firs <img src="/ltximg/functions_in_the_discrete_verse_5c5675d52459c6771546a81fe199866484ac36bc.png" alt="\(C_4\)"> t <img src="/ltximg/functions_in_the_discrete_verse_6dfab97b56a0d0d34f7a132afd27c15c8bafa601.png" alt="$C_4$"> to the same thing on the other <img src="/ltximg/functions_in_the_discrete_verse_6dfab97b56a0d0d34f7a132afd27c15c8bafa601.png" alt="$C_4$">


<div class="equation-container">
<span class="equation">
<img src="/ltximg/functions_in_the_discrete_verse_739a0f935328bd6376a61666b456afa83b7a3121.png" alt="\begin{tikzpicture}[
  vertex1/.style={draw,thin,circle,fill=Yellow,minimum size=14pt,inner sep=0pt},
  vertex2/.style={draw,thin,circle,fill=Thistle,minimum size=14pt,inner sep=0pt},
  action/.style={draw=VioletRed,thick,&amp;gt;={Stealth[round]}},
  action1/.style={draw=red,thick,&amp;gt;={Stealth[round]}},
  action2/.style={draw=blue,thick,&amp;gt;={Stealth[round]}},
  background rectangle/.style={fill=white},
  show background rectangle]
  \graph[math nodes]{
    {[clockwise,n=4,nodes={vertex1},edges=action1] 0,1,2,3; 0-&amp;gt;1-&amp;gt;2-&amp;gt;3-&amp;gt;0},
    {[clockwise,n=4,nodes={vertex2,xshift=3cm},edges=action2] 0',1',2',3'; 0'-&amp;gt;1'-&amp;gt;2'-&amp;gt;3'-&amp;gt;0'},
    {[edges=action,edge label=\color{VioletRed}$h_{1}$]
     0 -&amp;gt; 0',
     1 -&amp;gt;[bend left=30] 1',
     2 -&amp;gt; 2',
     3 -&amp;gt;[bend right=30] 3'}
  };
\end{tikzpicture}
">
</span>
<span class="equation-label">
1
</span>
</div>

The key to creating a function from this diagram is figuring how to translate from nodes on a graph to numbers, modulo something.
Using <img src="/ltximg/functions_in_the_discrete_verse_df237053ce50a8ea5bf6da01665cbb678f3e3247.png" alt="$\mathbb{Z}/4\mathbb{Z}$"> as our isomorphism, (read. renaming of) <img src="/ltximg/functions_in_the_discrete_verse_41426d7dccb2026dd22ba7f0a7b0b870548341e8.png" alt="$C_{4}$"> works wonders here!
The graph above corresponds to this table:

| <img src="/ltximg/functions_in_the_discrete_verse_32d3fa9c0e1a5f778c4ac24e0a1da0f4ac86e36e.png" alt="$n$"> | <img src="/ltximg/functions_in_the_discrete_verse_b8a33a95ee9ac801c093997e1149dee39fd46d3f.png" alt="$h_1(n)$"> |
|------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
| 0                                                                                                          | 0                                                                                                               |
| 1                                                                                                          | 1                                                                                                               |
| 2                                                                                                          | 2                                                                                                               |
| 3                                                                                                          | 3                                                                                                               |

which is a function <img src="/ltximg/functions_in_the_discrete_verse_c6beab6ae3e428c08c524dffbae16f3706820654.png" alt="$h_1(n) = n$">. A homomorphism on a group also always corresponds to a generator on that group. In this case. <img src="/ltximg/functions_in_the_discrete_verse_89ee06df0ef855b727a2ddbde0a24901e00f3d54.png" alt="$&amp;lt;1&amp;gt;$">

That's our easy function. We know from the problem statement that we have four to find. Three more to go.

Next up, we want to do something like this.


<div class="equation-container">
<span class="equation">
<img src="/ltximg/functions_in_the_discrete_verse_036e1596a63acf41dccb94811f530a11080e862f.png" alt="\begin{tikzpicture}[
  vertex1/.style={draw,thin,circle,fill=Yellow,minimum size=14pt,inner sep=0pt},
  vertex2/.style={draw,thin,circle,fill=Thistle,minimum size=14pt,inner sep=0pt},
  action/.style={draw=SeaGreen,thick,&amp;gt;={Stealth[round]}},
  action1/.style={draw=red,thick,&amp;gt;={Stealth[round]}},
  action2/.style={draw=blue,thick,&amp;gt;={Stealth[round]}},
  background rectangle/.style={fill=white},
  show background rectangle]
  \graph[math nodes]{
    {[clockwise,n=4,nodes={vertex1},edges=action1] 0,1,2,3; 0-&amp;gt;1-&amp;gt;2-&amp;gt;3-&amp;gt;0},
    {[clockwise,n=4,nodes={vertex2,xshift=3cm},edges=action2] 0',1',2',3'; 0'-&amp;gt;1'-&amp;gt;2'-&amp;gt;3'-&amp;gt;0'},
    {[edges=action,edge label=\color{SeaGreen}$h_{2}$]
     0 -&amp;gt; 0',
     1 -&amp;gt;[bend left=30] 3',
     2 -&amp;gt; 2',
     3 -&amp;gt;[bend left=80] 1'}
  };
\end{tikzpicture}
">
</span>
<span class="equation-label">
1
</span>
</div>

| <img src="/ltximg/functions_in_the_discrete_verse_32d3fa9c0e1a5f778c4ac24e0a1da0f4ac86e36e.png" alt="$n$"> | <img src="/ltximg/functions_in_the_discrete_verse_2d6ec4bdd0a1f457c674ac0acbfa91004206f31c.png" alt="$h_2(n)$"> |
|------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
| 0                                                                                                          | 0                                                                                                               |
| 1                                                                                                          | 3                                                                                                               |
| 2                                                                                                          | 2                                                                                                               |
| 3                                                                                                          | 1                                                                                                               |

Like the above, but reversed in a sense.
This corresponds to the homomorphism <img src="/ltximg/functions_in_the_discrete_verse_75339486a0cdd589f487320220f75c7f3a5f1fd2.png" alt="$h_2(n) = 4-n$"> which, remember, given that we're only working with positive numbers
mod 4, will just wrap around conventiently as we wish it to! This corresponds to the generator <img src="/ltximg/functions_in_the_discrete_verse_1ff8d8195e33d88211bf1df37c45f198cefb2daa.png" alt="$&amp;lt;3&amp;gt;$">

One nice thing to think about is the difference between the image of a function and vs. the codomain.
In the statement <img src="/ltximg/functions_in_the_discrete_verse_f7c60419ca206a9a3fb9a0cf341d460e6d8a0d57.png" alt="$h_i : C_4 \rightarrow C_4$">, the thing on the left of the arrow is the domain, elements from which you are allowed throw into the function.
The codomain is the thing on the right of the arrow.
The image of a function is subtly different: it's the set of values the function can push you to. Not all functions will do this, as seen above.
The homomorphisms above could both be considered **isomorphisms**, or as I like to think of them, **renaming functions**.
All isomorphisms are homomorphisms, but not all homomorphisms are isomorphisms, and the next example will show a nice example of that.


<div class="equation-container">
<span class="equation">
<img src="/ltximg/functions_in_the_discrete_verse_0cc6d3e2af778269a4251ba0f44b05ce02e2dd85.png" alt="\begin{tikzpicture}[
  vertex1/.style={draw,thin,circle,fill=Gray,minimum size=14pt,inner sep=0pt},
  vertex2/.style={draw,thin,circle,fill=DarkOrchid,minimum size=14pt,inner sep=0pt},
  vertex3/.style={draw,thin,circle,fill=Black,minimum size=14pt,inner sep=0pt},
  action/.style={draw=Black,thick,&amp;gt;={Stealth[round]}},
  action1/.style={draw=red,thick,&amp;gt;={Stealth[round]}},
  action2/.style={draw=blue,thick,&amp;gt;={Stealth[round]}},
  background rectangle/.style={fill=white},
  show background rectangle]
  \graph[math nodes]{
    {[clockwise,n=4,nodes={vertex1},edges=action1] 0,1,2,3; 0-&amp;gt;1-&amp;gt;2-&amp;gt;3-&amp;gt;0},
    {[counterclockwise,n=4,nodes={vertex2,xshift=3cm},edges=action2] 0'[vertex3],1'[vertex3],2',3'; 0'-&amp;gt;1'-&amp;gt;2'-&amp;gt;3'-&amp;gt;0'},
    {[edges=action,edge label=$h_{3?}$]
     0 -&amp;gt; 0',
     1 -&amp;gt;[bend left=30] 1',
     2 -&amp;gt;[bend right=60] 0',
     3 -&amp;gt;[bend right=20] 1'}
  };
\end{tikzpicture}
">
</span>
<span class="equation-label">
1
</span>
</div>

| <img src="/ltximg/functions_in_the_discrete_verse_32d3fa9c0e1a5f778c4ac24e0a1da0f4ac86e36e.png" alt="$n$"> | <img src="/ltximg/functions_in_the_discrete_verse_249da2d059b093f4254362398fc1a4df9068b06c.png" alt="$h_{3?}(n)$"> |
|------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
| 0                                                                                                          | 0                                                                                                                  |
| 1                                                                                                          | 1                                                                                                                  |
| 2                                                                                                          | 0                                                                                                                  |
| 3                                                                                                          | 1                                                                                                                  |

Here, I used a cute lil' result called Lagrange's Theorem, stated below.


<div class="equation-container">
<span class="equation">
<img src="/ltximg/functions_in_the_discrete_verse_a194051aba57e72d0eafb01bd67ebf6b10dcb0ec.png" alt="\begin{align}
\textbf{(Lagrange's Theorem (On Groups)). }\\
\scriptstyle{\text{If } H &amp;lt; G\,\Rightarrow \left| H}  \right| \bigg{\mid}\scriptstyle{ |G|}
\end{align}
">
</span>
<span class="equation-label">
1
</span>
</div>

If we have a group called G with any subgroup H, we are guaranteed that the size of H (otherwise called the order, in a group theory context)
divides the size of G.
Here, the order of G is 4. The divisors of 4 are ... 2 and 1. This means to look for subgroups in our domain, we can narrow our search to subgroups of order 2 or 1

The more shaded areas above are part of the **codomain** <img src="/ltximg/functions_in_the_discrete_verse_6dfab97b56a0d0d34f7a132afd27c15c8bafa601.png" alt="$C_4$"> but not the **image of <img src="/ltximg/functions_in_the_discrete_verse_cff9ba1b8978e0480ad087766e0c45f584beedb1.png" alt="$h_3$">**, henceforth <img src="/ltximg/functions_in_the_discrete_verse_8ba06588794e3b5743188ba1c98b018d1a8edb48.png" alt="$Im(h_3)$">.
The function I used was <img src="/ltximg/functions_in_the_discrete_verse_7ab16d62c6327b3ddbb9ab17345d00c65aca333f.png" alt="$h_3 = x mod 2$">.

There's a problem though. This doesnt correspond to a generator in <img src="/ltximg/functions_in_the_discrete_verse_6dfab97b56a0d0d34f7a132afd27c15c8bafa601.png" alt="$C_4$">!
**This is not a valid homomorphism on a group because the homomorphism's image is not a subgroup of <img src="/ltximg/functions_in_the_discrete_verse_6dfab97b56a0d0d34f7a132afd27c15c8bafa601.png" alt="$C_4$">**.

Small fix though. We point our arrows at one of the other pairs of things in our codomain.
One that's backed up by a generator. <img src="/ltximg/functions_in_the_discrete_verse_6c1d60fd187da59d4c8aca22e741565086919d27.png" alt="$&amp;lt;2&amp;gt;$"> probably...


<div class="equation-container">
<span class="equation">
<img src="/ltximg/functions_in_the_discrete_verse_b64f2c24ec22b61579dde61ca35178b0c0d19329.png" alt="\begin{tikzpicture}[
  vertex1/.style={draw,thin,circle,fill=Yellow,minimum size=14pt,inner sep=0pt},
  vertex2/.style={draw,thin,circle,fill=DarkOrchid,minimum size=14pt,inner sep=0pt},
  vertex3/.style={draw,thin,circle,fill=Rhodamine,minimum size=14pt,inner sep=0pt},
  action/.style={draw=Rhodamine,thick,&amp;gt;={Stealth[round]}},
  action1/.style={draw=red,thick,&amp;gt;={Stealth[round]}},
  action2/.style={draw=blue,thick,&amp;gt;={Stealth[round]}},
  background rectangle/.style={fill=white},
  show background rectangle]
  \graph[math nodes]{
    {[clockwise,n=4,nodes={vertex1},edges=action1] 0,1,2,3; 0-&amp;gt;1-&amp;gt;2-&amp;gt;3-&amp;gt;0},
    {[clockwise,n=4,nodes={vertex2,xshift=3cm},edges=action2] 0'[vertex3],1',2'[vertex3],3'; 0'-&amp;gt;1'-&amp;gt;2'-&amp;gt;3'-&amp;gt;0'},
    {[edges=action,edge label=\color{Rhodamine}$h_{3}$]
     0 -&amp;gt; 0',
     1 -&amp;gt;[bend right=50] 2',
     2 -&amp;gt;[bend left=30] 0',
     3 -&amp;gt;[bend right=30] 2'}
  };
\end{tikzpicture}
">
</span>
<span class="equation-label">
2
</span>
</div>

<img src="/ltximg/functions_in_the_discrete_verse_9da033d21716ed7128abf35c2f7f7549834f506e.png" alt="$h_4$">

| <img src="/ltximg/functions_in_the_discrete_verse_32d3fa9c0e1a5f778c4ac24e0a1da0f4ac86e36e.png" alt="$n$"> | <img src="/ltximg/functions_in_the_discrete_verse_62f4dbe138cb8bcc7e70d6c5b8765aba854731d3.png" alt="$h_{3}(n)$"> |
|------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| 0                                                                                                          | 0                                                                                                                 |
| 1                                                                                                          | 2                                                                                                                 |
| 2                                                                                                          | 0                                                                                                                 |
| 3                                                                                                          | 2                                                                                                                 |


<div class="equation-container">
<span class="equation">
<img src="/ltximg/functions_in_the_discrete_verse_d3ee5c2898b26b9cb9eba677a1c626cfea36ae7e.png" alt="\begin{tikzpicture}[
  vertex1/.style={draw,thin,circle,fill=Goldenrod,minimum size=14pt,inner sep=0pt},
  vertex2/.style={draw,thin,circle,fill=DarkOrchid,minimum size=14pt,inner sep=0pt},
  vertex3/.style={draw,thin,circle,fill=Melon,minimum size=14pt,inner sep=0pt},
  action/.style={draw=Melon,thick,&amp;gt;={Stealth[round]}},
  action1/.style={draw=red,thick,&amp;gt;={Stealth[round]}},
  action2/.style={draw=blue,thick,&amp;gt;={Stealth[round]}},
  background rectangle/.style={fill=white},
  show background rectangle]
  \graph[math nodes]{
    {[clockwise,n=4,nodes={vertex1},edges=action1] 0,1,2,3; 0-&amp;gt;1-&amp;gt;2-&amp;gt;3-&amp;gt;0},
    {[clockwise,n=4,nodes={vertex2,xshift=3cm},edges=action2] 0'[vertex3],1',2',3'; 0'-&amp;gt;1'-&amp;gt;2'-&amp;gt;3'-&amp;gt;0'},
    {[edges=action,edge label=\color{Melon}$h_{4}$]
     0 -&amp;gt; 0',
     1 -&amp;gt; 0',
     2 -&amp;gt;[bend left=270] 0',
     3 -&amp;gt;[bend right=270] 0'}
  };
\end{tikzpicture}
">
</span>
<span class="equation-label">
2
</span>
</div>

| <img src="/ltximg/functions_in_the_discrete_verse_32d3fa9c0e1a5f778c4ac24e0a1da0f4ac86e36e.png" alt="$n$"> | <img src="/ltximg/functions_in_the_discrete_verse_81e7ad0c14d1fe841fbe2bdffecbff907961c484.png" alt="$h_{4}(n)$"> |
|------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| 0                                                                                                          | 0                                                                                                                 |
| 1                                                                                                          | 0                                                                                                                 |
| 2                                                                                                          | 0                                                                                                                 |
| 3                                                                                                          | 0                                                                                                                 |

It took me embarrasingly long to figure out, but by definition, this is a homomorphism.
And there we go! all the homomorphisms going from <img src="/ltximg/functions_in_the_discrete_verse_f7c60419ca206a9a3fb9a0cf341d460e6d8a0d57.png" alt="$h_i : C_4 \rightarrow C_4$">.