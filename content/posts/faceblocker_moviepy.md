+++
title = "faceblocker: a tool for removing faces from videos"
author = ["Ri\\*"]
date = 2023-12-06T00:00:00+00:00
draft = false
+++

## Replace your face from the terminal! {#replace-your-face-from-the-terminal}

<https://gitlab.com/rdotcollins/movie-anonymiser>


### installing the CLI tool itself {#installing-the-cli-tool-itself}

```bash
pipx install faceblocker
```

And then, you'll be able to take videos like this...

{{< figure src="/test_lsoh.gif" >}}

...specify an image using the -p parameter and say the magic words like so...

```bash
faceblocker -i ./inputvideo.mp4 -p ./fox.png --samplerate 2 --scale 0.3 --nms_threshhold -o test_lsoh_out.mp4
```

creating...

{{< figure src="/test_lsoh_out.gif" >}}


### pyproject and MANIFEST {#pyproject-and-manifest}

```cfg
[project]
name = "faceblocker"
version = "0.0.4"
authors = [
{name="Rita Collins", email="r.collins3730@gmail.com" },
]
description = "A CLI tool that can remove faces from videos and replace them with images."
readme = "README.md"
requires-python = ">=3.11"
dependencies = [
"moviepy<2.0",
"opencv-python>4.8.0",
]
classifiers = [
"Programming Language :: Python :: 3",
"License :: OSI Approved :: MIT License",
"Operating System :: OS Independent",
]

[project.urls]
"Homepage" = "https://gitlab.com/rdotcollins/movie-anonymiser"
"Bug Tracker" = "https://gitlab.com/rdotcollins/movie-anonymiser/issues"

#[tool.setuptools.packages]
#find = {}  # Scan the project directory with the default parameters
[tool.setuptools]
include-package-data = true
# OR
[tool.setuptools.package-data]
"faceblocker.models" = ["*"]


[tool.setuptools.dynamic]
readme = {file = ["README.org"]}
[tool.setuptools.packages.find]
# All the following settings are optional:
namespaces = true
where = ["src"]  # ["."] by default
include = ["*"]  # ["*"] by default

[project.scripts]
faceblocker = "faceblocker.deface:main"

[build-system]
requires = ["setuptools >= 61.0"]
build-backend = "setuptools.build_meta"


```

```cfg
# Command Description
# include pat1 pat2 ... Add all files matching any of the listed patterns (Files must be given as paths relative to the root of the project)
# exclude pat1 pat2 ... Remove all files matching any of the listed patterns (Files must be given as paths relative to the root of the project)
# recursive-include dir-pattern pat1 pat2 ... Add all files under directories matching dir-pattern that match any of the listed patterns
recursive-include ./yunet_model/*
include test_lsoh_out.gif
# recursive-exclude dir-pattern pat1 pat2 ... Remove all files under directories matching dir-pattern that match any of the listed patterns
# global-include pat1 pat2 ... Add all files anywhere in the source tree matching any of the listed patterns
# global-exclude pat1 pat2 ... Remove all files anywhere in the source tree matching any of the listed patterns
# graft dir-pattern Add all files under directories matching dir-pattern
# prune dir-pattern Remove all files under directories matching dir-pattern
# The patterns here are glob-style patterns: * matches zero or more regular filename characters (on Unix, everything except forward slash; on Windows, everything except backslash and colon); ? matches a single regular filename character, and [chars] matches any one of the characters between the square brackets (which may contain character ranges, e.g., [a-z] or [a-fA-F0-9]). Setuptools also has undocumented support for ** matching zero or more characters including forward slash, backslash, and colon.
# Directory patterns are relative to the root of the project directory; e.g., graft example* will include a directory named examples in the project root but will not include docs/examples/.
```


### what does it look like? {#what-does-it-look-like}

```bash
cd /home/river/werk/projects/movie_anonymiser
pwd
tree -L 2 ./ -I "envs|build"
```

```text
/home/river/werk/projects/movie_anonymiser
./
├── bld.bat
├── build.sh
├── construct.yaml
├── dist
│   ├── faceblocker-0.0.4-py3-none-any.whl
│   └── faceblocker-0.0.4.tar.gz
├── environment.yml
├── faceblocker.sh
├── freeblurred-0.1-Linux-x86_64.sh
├── LICENCE
├── MANIFEST.in
├── meta.yaml
├── old
│   ├── freeblurred_2.py
│   ├── freeblurred_old.py
│   └── setup.py
├── pyproject.toml
├── README.md
├── README.org
├── requirements.txt
├── src
│   ├── faceblocker
│   └── faceblocker.egg-info
├── testdata
│   ├── in
│   └── out
├── test_lsoh_out.gif
└── tmp

9 directories, 19 files
```


### installing it as a library {#installing-it-as-a-library}

If you want to download and run it from source, do

```bash
pip install faceblocker
```

and then run faceblocker using

```bash
python3 -m faceblocker
```

If things are working right with the script and the virtual environment you (Hopefully!) installed this into,
`faceblocker` should be in your `$PATH` and should allow for being called with just

and then run faceblocker using

```bash
faceblocker
```


### Usage {#usage}

```bash
faceblocker -h
```

so as an example,

```bash
faceblocker -i inputvideo.mp4
```

should take `inputvideo.mp4`, render a black square over all faces detected using the default settings on the model.
The default settings are fairly liberal in considering things "faces" and will often block first, ask questions later.
The default parameters are set to the following.

| parameter       | default |                                                                                                                                                                                    |
|-----------------|---------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| --scale         | 0.4     | For speed reasons, face detection is done on a resized version of the frame in memory. 0.5 means faces are detected on a frame half the width and height of the initial frame.     |
| --score         | 0.3     | The cutoff confidence score of any bounding boxes. Any boxes that is less likely than this to be a face are discarded                                                              |
| --nms_threshold | 0.1     | The "interval over union" score of any bounding boxes with respect to the training data. Any boxes with an IOU score less than this are discarded.                                 |
| --n_kboundboxes | 1000    | The number of bounding boxes generated per frame                                                                                                                                   |
| --samplerate    | 5       | This script will sample every k'th frame of the video to detect faces on it, use this to specify how often it will attempt to detect a face. Higher is more accurate.              |
| --blockfactor   | 1.5     | Where a face is detected, how much should the height and width of the bounding box of the covering image be scaled?                                                                |
| --unstretch     | False   | Due to an upstream bug in moviepy, some vertical videos are automatically rescaled horizontally and will look strangely wide. Set this flag to "True" to stop this from happening. |


### pypi and building {#pypi-and-building}

Given the directory structure, this is a standard python package with a `./src/<packagename>` folder.
The wheels can be built from the source and uploaded with `twine`, another standard python package that standardizes PyPI uploads.


#### uploading to pypi {#uploading-to-pypi}

```bash
python3 -m twine upload --repository pypi dist/*
```
