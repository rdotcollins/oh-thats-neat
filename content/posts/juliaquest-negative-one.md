+++
title = "JULIAQUEST 1:"
author = ["river"]
date = 2020-11-12T00:00:00+00:00
draft = false
+++

## So I've been learning some Julia. {#so-i-ve-been-learning-some-julia-dot}

Over the lockdown I told myself to broaden my computuational horizons and do some cool programming stuff.
I chose Julia because I thought a language designed for numerical work and data science from the ground up sounded cool.
I'll save you the usual sales pitch about generic programming and multiple dispatch,
and take the idea that it's more performant than python and R at some stuff most of the time as a given.

What's it like to learn?


### Going off-road {#going-off-road}

A big reason I wanted to learn Julia was the idea of doing off-road data science.
Far from doing statistics in the back of a quad-bike, I found most of the interesting work done with data done on messy data.
Interesting stuff comes from pre-cleaned data, but with the elegance of modelling and the stats behind it, came datasets I felt just arrived ex-nihilo pretty and ready to analyse, its secrets spoiled
on the first pivot table.

I like cleaning data. It feels like archaeology, digging out structure and meaning one badly-formatted .json file at a time.
Cleaning data you're due to analyse makes you think about the questions you're trying to answer.

-   What am I looking for?
-   What does the structure of the data tell me? How does this data want to be represented?
-   Where did this data come from, am I seeing the whole picture?
-   What relates this data to the real world?
-   How can I best summarise it?
-   Are there clear independant and dependant variables?
-   Is there metadata?
-   What even is a .json file (cue montage of googling the word "tidyr vignette rectangling")

If python is a hiker walking over a mountain, and R (or sufficiently vectorised python) is a car driving along the winding roads across it:
Then that makes Julia a go-anywhere ATV, rugged and ready. The road less travelled, as a language.
I like that.


## How am I going about learning this? {#how-am-i-going-about-learning-this}

One day I tried to look up a way to learn `emacs-lisp` and found a nice way of learning languages through the
tried and tested rigours of test-driven development (TDD). It was [Thomas Moulia et al's Elisp Koans](https://github.com/jtmoulia/elisp-koans).
They were great! It was essentially a workbook of coding exercises in a Elisp that gradually increased in difficulty as your knowledge grew.
Knowing how well it worked to help me learn other languages, I looked around the internet for another similar set of problems for julia when I hit the motherlode: [exercism](https://exercism.io/faqs).
Their exercises are pretty handy and the connection with people who really know Julia made it an ideal place for me to start really getting to grips with the language.

(I'm not affiliated with them in any way - I'm not even using their paid tier)

Anyway, onto the problems.


## My first nice problem: Pascal's triangle {#my-first-nice-problem-pascal-s-triangle}

This was one of the first problems that threw me for a loop trying to learn Julia.

Pascal's triangle is one of those things you do need to brush the rust from if you're not actively doing combinatorics in your spare time.
Heres a wikipedia link.
<https://en.wikipedia.org/wiki/Pascal%27s%5Ftriangle>

This is how I did it.


### As a function of the row above it {#as-a-function-of-the-row-above-it}

An approach I first remembered was that each entry of the triangle was the sum of the two entries above it.
But that tactic didn't help much.

```julia
#doodles
row = [
[1], #n = 1
[1,1],#n = 2
[1,2,1]#n = 3, okay the iteration makes sense now
]
```

But I then thought about trying to define the "entries" to the left and right of row 1, for example, and realised
"hmm, that's a rather inconvenient amount of edge cases"

So I scrapped the iterative approach here.


### As a demonstration of how combinations work {#as-a-demonstration-of-how-combinations-work}

A bright light flashed in my head. I was transported back to the leaving cert.
I remember thinking to myself back then:

"oh hey i dont need to do stupid en-see-arr stuff at all! i just wish i could bring this triangle into the exam..."

Thanks, leaving cert flashback.

So each row of Pascal's triangle represents the binomial coefficients of the binomial expansion

\begin{equation\*}
(1+x)^n = \sum\_{i=0}^{n} \binom{n}{k} x^k
\end{equation\*}

for each row representing an expansion with a different \\(n\\).
Because the binomial term \\(\binom{n}{k}\\) was one when \\(n=k\\) and \\(n=0\\), you had a pattern arise like

\begin{eqnarray\*}
&(1+x)^0 &= 1 \\\\\\
&(1+x)^1 &= 1 + 1x \\\\\\
&(1+x)^2 &= 1 + 2x + 1x^2 \\\\\\
&(1+x)^3 &= 1 + 3x + 3x^2 + 1x^3\\\\\\
\end{eqnarray\*}

Rub those x's out (we're only worried about coefficients here) and you have the exact pattern we're looking for. Nice.
Lines are parameterised by \\(n\\)... don't know why I forgot that.

```julia
#I use array comprehensions here because I think they're neat.
function triangle(n)
    line(n) = [binomial(n,k) for k in 0:n]
    [line(j) for j in 0:n-1]
end

triangle(0)
triangle(10)
```

```text
10-element Array{Array{Int64,1},1}:
 [1]
 [1, 1]
 [1, 2, 1]
 [1, 3, 3, 1]
 [1, 4, 6, 4, 1]
 [1, 5, 10, 10, 5, 1]
 [1, 6, 15, 20, 15, 6, 1]
 [1, 7, 21, 35, 35, 21, 7, 1]
 [1, 8, 28, 56, 70, 56, 28, 8, 1]
 [1, 9, 36, 84, 126, 126, 84, 36, 9, 1]
```


### Exercism says I'm wrong!!?? {#exercism-says-i-m-wrong}

Yeah, that's fair.
Sometimes its easy to forget that solutions to problems are rarely so exact.
I'm not making a Pascal's triangle, I'm making a function that something will ingest and use.
And that means I can't just forget the corner cases.

So this means I get to show off one of the flashier things about Julia: short circuit evaluation.

```julia
x = true
x || print("x is false, sound the alarms!!!")
```

```text
true
```

This is neat. An implicit boolean statement is happening here.
\\(x \; \textbf{OR}\; \text{true} \implies \text{false}\\)

It seems arbitrary code in Julia is kinda "truthy" - it can act as `true` in a boolean statement.
When x is true, the boolean statement **cannot** be true, which means that the code on the right of the `||` operator will never be executed!
When x is false...

```julia
x = false
x || print("x is false, sound the alarms!!!")
```

```text
x is false, sound the alarms!!!
```

The code to the right of the `||` operator is run, and a warning string is printed.
This facet of Julia is undoubtedly going to make something important of mine bug out in a weird way sometime in the future, but for now it lets me do cool things like this.

```julia
#I use array comprehensions here because I think they're neat.
function triangle(n)
#nice unicode support: if n is less than 0, the domainerror gets thrown
    n ≥ 0 || throw(DomainError("pascal's triangle defined for n ≥ 0 "))

    row(n) = [binomial(n,i) for i in 0:n]
    [row(j) for j in 0:n-1]
end

triangle(-1)
```

```text
DomainError with pascal's triangle defined for n ≥ 0 :


Stacktrace:
 [1] triangle(::Int64) at ./In[86]:4
 [2] top-level scope at In[86]:10
 [3] include_string(::Function, ::Module, ::String, ::String) at ./loading.jl:1091
 [4] execute_code(::String, ::String) at /home/river/.julia/packages/IJulia/rWZ9e/src/execute_request.jl:27
 [5] execute_request(::ZMQ.Socket, ::IJulia.Msg) at /home/river/.julia/packages/IJulia/rWZ9e/src/execute_request.jl:86
 [6] #invokelatest#1 at ./essentials.jl:710 [inlined]
 [7] invokelatest at ./essentials.jl:709 [inlined]
 [8] eventloop(::ZMQ.Socket) at /home/river/.julia/packages/IJulia/rWZ9e/src/eventloop.jl:8
 [9] (::IJulia.var"#15#18")() at ./task.jl:356
```

DomainError? Check.


## A hairier problem: The Spiral Matrix. {#a-hairier-problem-the-spiral-matrix-dot}

My first problem was how to navigate around a square matrix in a "spiral" pattern.
The problem was to create a function of just an integer, \\(n\\) such that, for example...

\begin{align\*}
\textbf{spiral\\_matrix}(5) =
\left(
\begin{array}{ccccc}
\color{red}{1}&\color{red}{2}&\color{red}{3}&\color{red}{4}&\color{red}{5}\\\\\\
\color{blue}{16}&17&18&19&\color{Green}{6}\\\\\\
\color{blue}{15}&24&25&20&\color{Green}{7}\\\\\\
\color{blue}{14}&23&22&21&\color{Green}{8}\\\\\\
\color{VioletRed}{13}&\color{VioletRed}{12}&\color{VioletRed}{11}&\color{VioletRed}{10}&\color{Green}{9}\\\\\\
\end{array}
\right)
\end{align\*}

I had a lot of trouble trying to find how to "save an index" in julia -
how to construct a path around a matrix in terms of \\(n\\) that a matrix would be able to understand.
but I found this cute little nugget in the manual:
you can use . with CartesianIndex (the constructor) to broadcast it over a collection of ranges in the constructor!

```julia
"""
returns an array of cartesian indices used to traverse the outside of an nxn matrix, clockwise,
starting at the top left, and ending just below the top left.
"""
#A note to future me: CartesianIndex(1,1:n) won't do what you want it to.

peelidx(n) = [CartesianIndex.(1,1:n); #left to right, across the top::arrow-right:
              CartesianIndex.(2:n,n); #top to bottom along the right: :arrow-down:
              CartesianIndex.(n,n-1:-1:1); #right to left, along the bottom: :arrow-left:
              CartesianIndex.(n-1:-1:2,1)] #bottom to top along the left: :arrow-up:

```

Note that when we're back at \\(16\\), the next number we go to - \\(17\\) - is back at the upper right corner of a matrix.
We can partition the "motion" of indexing around the matrix in a spiral by defining
a "peeling" of a matrix, where the coloured part of the matrix above is the "peel" of the matrix, and
the rest is a core - again a square matrix! We can keep peeling the matrix until it's just a \\(1\times 1\\) or \\(2\times 2\\) matrix.

`peelidx(n)` finds where the peel is, but how do we assign numbers to it?
Julia, much aquainted with the rigors of doing things to boxes of numbers, allows for us to "view" arrays.
This allows us to grab a particular part of the matrix **and assign directly to it without copying the entire matrix!** using a macro!
In comparison to R's tendency to copy things gratuitously as a conscious design choice, Julia gives you the option: views \\(OR\\) copies.

Note: the `!` on the back of the function is a Julia custom that indicates that the function mutates the object.
`peelmatrix` doesnt mutate the matrix, but it offers views with which to immediately do that, so I think it's in good taste.

```julia
"""
Returns two views on a n x n matrix M
The peel: An array view on M that corresponds to the outer elements of M
(elements at indices containing 1 or n)
The core: A n-2 x n-2 array view on M that can be seen as the complement of the peel.
Note: this returns *views* on the matrix M, not copies of the matrix itself.
While no modification happens to M in this function, the fact that this returns views of the input lends itself to immediately changing M afterwards - probably doesn't warrant the ! at the end but it's close enough I think. Name criticism welcomed :D
"""
function peelmatrix!(M)
    n = size(M)[1] #I want to avoid using the end symbol in arrays here because CartesianIndex. isnt comfortable with it. We'll carry around the n here explicitly.
    peelindex = peelidx(n)
    peel = @view M[peelindex]
    core = @view M[2:n-1,2:n-1]
    peel,core #returns a tuple, the outside of the matrix, and the inside.
end
```

Now that we know how to decompose the problem, we solve it by
assigning the values \\(1:n\\) to successive "peels" of a blank matrix.

```julia
"""
Returns an nxn matrix with the values from 1 to n^2
arranged in a counterclockwise spiral around n^2
"""
function spiral_matrix(n)
    values = collect(1:n^2) #collected into an array because splice! doesnt like ranges that much.
    output_matrix = zeros(Int64,n,n) #initialise
    #peel_collection = [] #for debugging's sake
    peel,core = peelmatrix!(output_matrix) #first pass - stops the trivial spiral_matrix(1) failing
     #splice allows me to "cut" from values and "paste" into peel, which assigns the numbers in the spiral in order!
    peel .= splice!(values,1:length(peel))
    #display("peel = $(peel) \n\n, core = $(core)\n\n") #for debugging's sake
    #push!(peel_collection,peel)
    while length(core)>0
        peel,core = peelmatrix!(core)
        peel .= splice!(values,1:length(peel))
        #display("peel = $(peel) \n\n, core = $(core)\n\n")
        #push!(peel_collection,peel)
    end
    output_matrix
end
for n in 4:6
display(spiral_matrix(n))
end
```

```text
4×4 Array{Int64,2}:
  1   2   3  4
 12  13  14  5
 11  16  15  6
 10   9   8  7
5×5 Array{Int64,2}:
  1   2   3   4  5
 16  17  18  19  6
 15  24  25  20  7
 14  23  22  21  8
 13  12  11  10  9
6×6 Array{Int64,2}:
  1   2   3   4   5   6
 20  21  22  23  24   7
 19  32  33  34  25   8
 18  31  36  35  26   9
 17  30  29  28  27  10
 16  15  14  13  12  11
```

Hooray!
It's not much of a quest, but like all reasonably sized journies, they all begin with single steps.
I'll discuss what kind of messes I got into with the type system in another post soon.
In the meantime, thanks for reading!
