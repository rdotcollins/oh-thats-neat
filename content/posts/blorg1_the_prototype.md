+++
title = "Blogging: a series of tubes to call my own"
author = ["Ri\\*"]
date = 2023-09-17T00:00:00+01:00
draft = false
+++

## Not saying its rocket science, but I'm proud of this little setup. {#not-saying-its-rocket-science-but-i-m-proud-of-this-little-setup-dot}

Blogging is as much a technical challenge as it is a creative one for me.
Because of how I understand things, I find the entire discipline of web design beautiful, wonderous and

**absolutely overwhelming**

and take diving rolls away from anything that even looks like css.


### So how the heck do I have a blog anyhow? {#so-how-the-heck-do-i-have-a-blog-anyhow}

In this day and age, the open source community at large have come up with ways to convert files to other files in ways that go from the elegant
(see pandoc).
My publishing workflow looks like this. There is definitely nicer ways to do this, but this works.


<div class="equation-container">
<span class="equation">
<img src="/ltximg/blorg1_the_prototype_b4058869a8683bd684bb04e98c5b2cc37434b06c.png" alt="\begin{tikzpicture}[
&amp;gt;=stealth,shorten &amp;gt;=3pt,draw=black,
nodes/.style = {draw, rectangle},
background rectangle/.style = {fill=white}, show background rectangle,
myarrows/.style={line width=1mm,draw=black, postaction={draw, line width=30mm, shorten &amp;gt;=4mm}}]
\graph[grow down sep=30,branch left sep,edge quotes=auto,
nodes={rectangle,minimum width=0.5cm, minimum height=0.5cm, text centered, text width=5cm, draw=black, fill=green!50}]
{
\texttt{.org} file from Org-Mode -&amp;gt;[red,\texttt{ox-hugo}]
\texttt{.md} file in Blackfriday markdown -&amp;gt;[blue,\texttt{hugo}]
\texttt{.html + .css} website
};
\end{tikzpicture}
">
</span>
<span class="equation-label">
1
</span>
</div>

...actually its more like this.

{{< figure src="/static/stack.png" >}}

In short,

-   I code things in org mode to make sense of things and execute things code block by code block
-   When I want to take advantage of things like more advanced debuggers, or perform actions on raw source files, I can perform a tangling action from `Org-mode` to append all the code-blocks into an executable source file and execute it from there.
-   Often for data analysis or mathematically savvy work, it helps to be able to represent things in pictures and graphs, and so `.org` files make a lot of sense to work in.
-   However for acts of creating what may be defined as "software", for mass use, I default to displaying the code in `org-mode` for display purposes but evaluating it elsewhere.
-   Rendering this code is mostly taken care of by Hugo and it's access to packages like `pygments` to prettify my code and creative commons theming that the Hugo community at large have contributed. Its wonderful.
